﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Ddd.Customers.Core.Domain.Aggregates.Customer
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Customer
    {
	    private readonly List<Contact> _contacts = new List<Contact>();
	    private readonly List<JobPlace> _jobPlaces = new List<JobPlace>();
	    
	    public Guid Id { get; private set; }

	    public string FullName { get; private  set; }

	    public AcquisitionChannel Channel { get; private  set; }
	    
	    public DateTime CreatedDate { get; private  set; }
	    
	    public bool IsActive { get; private  set; }
	    
	    public IEnumerable<JobPlace> JobPlaces => _jobPlaces.ToList();
	    
        public IEnumerable<Contact> Contacts => _contacts.ToList();

        public Customer(string fullName, AcquisitionChannel acquisitionChannel, DateTime createdDate)
        {
	        Id = Guid.NewGuid();
	        FullName = fullName;
	        Channel = acquisitionChannel;
	        CreatedDate = createdDate;
	        IsActive = true;
        }

        public void AddJobPlaces(List<JobPlace> jobPlaces)
        {
	        if (jobPlaces != null && jobPlaces.Any())
	        {
		        _jobPlaces.AddRange(jobPlaces);   
	        }
        }
        
        public void AddContacts(List<Contact> contacts)
        {
	        if (contacts != null && contacts.Any())
	        {
		        _contacts.AddRange(contacts);   
	        }
        }
    }
}

