﻿﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
  using Microsoft.EntityFrameworkCore;
  using Otus.Teaching.Ddd.Customers.Core.Domain.Aggregates.Customer;
  using Otus.Teaching.Ddd.Customers.Core.Infrastructure.DataAccess;
  using Otus.Teaching.Ddd.Customers.Domain.Repositories;

  namespace Otus.Teaching.Ddd.Customers.Infrastructure.Repositories
{
    public class EfCustomerRepository
        : ICustomerRepository
    {
        private readonly DataContext _dbContext;

        public EfCustomerRepository(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await _dbContext.Customers
                .Include(x => x.JobPlaces)
                .Include(x => x.Contacts)
                .ToListAsync();
        }
        
        public async Task<Customer> GetByIdAsync(Guid id)
        {
            return await _dbContext.Customers
                .Include(x => x.JobPlaces)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task AddAsync(Customer entity)
        {
            _dbContext.Customers.Add(entity);

            await _dbContext.SaveChangesAsync();
        }
        
        public async Task UpdateAsync(Customer entity)
        {
            _dbContext.Customers.Update(entity);

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid entityId)
        {
            var entity = await _dbContext.Customers.SingleOrDefaultAsync(x => x.Id == entityId);

            _dbContext.Customers.Remove(entity);

            await _dbContext.SaveChangesAsync();
        }
    }
}
